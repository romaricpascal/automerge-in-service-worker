# Automerge in Service Workers

Examples running [Automerge](https://automerge.org/) and [Automerge Repo](https://automerge.org/docs/repositories/) inside a [Service Worker](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API).

## Table of contents

* [Working examples](#working-examples)
* [Why?](#why)
* [Files of interest](#files-of-interest)
* [Technical constraints](#technical-constraints)
* [License](#license)

## Working examples

This repo provides two example pages. Both run a little counter whose state is held in a Service Worker:

1. [running Automerge](https://romaricpascal.gitlab.io/automerge-in-service-worker/examples/automerge) (no persistence, if you restart your browser, back to the initial state)
2. [running Automerge Repo with IndexedDB Storage](https://romaricpascal.gitlab.io/automerge-in-service-worker/examples/automerge) (persisted to IndexedDB, so state remains between restarts)

## Why?

Working with [Automerge Repo](https://automerge.org/docs/repositories/) to store images,
I experimented storing the as data URLs to allow passing them easily to `<img>` tags later on
for display. With Automerge running in the page JavaScript page, and a couple of images to access, I've found the thread blocked for a little bit, making the page janky.

Guess images data URLs are quite heavy strings to work around for large-ish images. There might be other options for storing/retrieving the images, maybe using other data types, which may be more efficient. But the alternative route that came to mind was: what if automerge was not runing in the page's thread, but in a Service Worker?

The worker's `fetch` event would allow to respond to regular `https://` URLs set as `<img>` tag `src`.
And `File` could be passed as `message` to the worker so that even the data URL conversion happens off the main thread.

And it also fits well with my tinkering of running offline-first app via a [Client-side Server](https://romaricpascal.gitlab.io/client-side-server/) (rather than a Single Page App).

That's the endgame, though, which I might explore in further updates/a separate repository.
The first step, and focus of this repository, is running Automerge or Automerge Repo from within a Service Worker.
And that's its [own set of challenges](#technical-constraints)

## Files of interest

* [The commented Rollup config](/rollup.config.js) should explain the plugins to set up for bundling a file that can be used through `importScripts` in a Service Worker
* [The example's `service-worker.js` and `index.html` file](/examples/) also have comments detailing how the page's code, service worker events and loading of the Web Assembly module work together. The [example running only automerge](/examples/automerge/) makes a good starting point, before looking a ]the one running automerge-repo and IndexedDB storage]\(/examples/automerge-repo/)

## Technical constraints

Running automerge inside a Service Worker presented the following challenges:

1. [Not all browsers support modules in Service Workers](https://web.dev/articles/es-modules-in-sw#backwards_compatibility), this means some bundling is required to avoid `import`
2. Internally, automerge relies on a [Web Assembly](https://developer.mozilla.org/en-US/docs/WebAssembly) module, compiled from Rust: [`@automerge/automerge-wasm`](https://www.npmjs.com/package/automerge-wasm). WebAssembly can be used from inside Workers like a Service Worker and bundlers have plugins to handle `.wasm` files. But there are some more things to work around:
   1. First one is that our Service Worker will not be importing the Web Assembly module directly. `@automerge/automerge-wasm` is used under the hood by `@automerge/automerge`, itself used under the hood by `@automerge/automerge-repo`. That means it'll be the bundler's job to ensure things load OK.
   2. The WebAssembly module is fairly large (1.5+Mb). A trade-off I'm fine with for building an offline-first app, where it will be cached after an initial download. However, there's a 4Kb limit for loading a Web Assembly module synchronously(see this note in \[`@rollup/plugin-wasm`]\((https://www.npmjs.com/package/@rollup/plugin-wasm#synchronous-modules), as well as [this comment on the ServiceWorker spec repository](https://github.com/w3c/ServiceWorker/issues/1407#issuecomment-578663018) or [this issue for discussing the limit](https://github.com/w3c/ServiceWorker/issues/1499))). This mean the bundling will need to produce either some `async` function or a Promise to `await` in the Service Worker code.
   3. Top-level await seems to only be available in a Service Worker running an ES Module... which not all browsers support.
      An simple exit for that one is to wrap the code of the worker in an asynchronous [IIFE](https://developer.mozilla.org/en-US/docs/Glossary/IIFE).
   4. Because the WebAssembly module is loaded asynchronously, that means the Service Worker becoming `active` doesn't necessarily mean it's ready to be interacted with. Pages will need to ask the Service Worker whether it's actually ready before trying to do anything with it. Service Worker side, this means registering events like `message` early on so the clients can communicate, but holding becoming `active` until that happens.
   5. Service Worker scripts are long running, but won't resist your browser getting closed. That means everything in memory vanishes and needs re-creating when the Service Worker is restarted... and holding telling pages it's ready until everything is correctly loaded.

## License

MIT, Copyright (c) 2023 Romaric Pascal
