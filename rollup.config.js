import { wasm } from '@rollup/plugin-wasm';
import resolve from '@rollup/plugin-node-resolve';
import topLevelAwait from 'rollup-plugin-tla';
import alias from '@rollup/plugin-alias';
import replace from '@rollup/plugin-replace';
import commonjs from '@rollup/plugin-commonjs';

console.log('Building for deployment in', process.env.CI_PROJECT_NAME);

const BUNDLING_OPTIONS = {
  // Fixes the `this` instance at top level in automerge's `stable.js` file
  context: 'globalThis',
  plugins: [
    // Force automerge-wasm to resolve to its web version
    alias({
      entries: [
        {
          find: '@automerge/automerge-wasm',
          replacement: 'node_modules/@automerge/automerge-wasm/web/index.js',
        },
      ],
    }),
    // Ensure we call the `WASM` loading function created by `@rollup/plugin-wasm`
    // See https://www.npmjs.com/package/@rollup/plugin-wasm#using-with-wasm-bindgen-and-wasm-pack
    // The `initSync` function doesn't need awaiting
    replace({
      preventAssignment: true,
      delimiters: ['', ''],
      values: {
        'initSync(WASM);': 'initSync(await WASM());',
      },
    }),
    // `automerge-repo` brings some instances of `process.env` within its code
    // which we need to replace as `process` is not available inside the browser
    replace({
      preventAssignment: true,
      values: {
        'process.env.NODE_ENV':
          process.env.NODE_ENV ?? JSON.stringify('production'),
        'process.env.DEBUG': process.env.DEBUG ?? 'false',
      },
    }),
    // Correctly handle the top-level await just introduced by the replacement
    // This will make the export be a Promise for the automerge module
    // which can then be accessed by `await AutomergePromise`
    topLevelAwait(),
    // We're bundling for a browser environment
    resolve({
      browser: true,
    }),
    // For `automerge-repo`'s `EventEmitter` dependency
    commonjs(),
    // And similarly the WASM file will be used in a browser as well
    wasm({
      targetEnv: 'browser',
      // Examples are served from `examples/<EXAMPLE_NAME>`
      // While the WASM will end up in `dist`
      publicPath: process.env.CI_PROJECT_NAME
        ? `/${process.env.CI_PROJECT_NAME}/dist/`
        : '/dist/',
    }),
  ],
};

export default [
  {
    input: '@automerge/automerge',
    output: {
      name: 'AutomergePromise',
      file: `dist/automerge-service-worker.js`,
      // Not all browsers support module Service Workers
      // so we'll bundle to an `iife`. That'll allow the final script
      // to run through `importScripts`
      format: 'iife',
    },
    ...BUNDLING_OPTIONS,
  },
  {
    input: '@automerge/automerge-repo',
    output: {
      name: 'AutomergeRepoPromise',
      file: `dist/automerge-repo-service-worker.js`,
      format: 'iife',
    },
    ...BUNDLING_OPTIONS,
  },
  {
    input: '@automerge/automerge-repo-storage-indexeddb',
    output: {
      name: 'AutomergeRepoStorageIndexedDBPromise',
      file: `dist/automerge-repo-storage-indexeddb.js`,
      format: 'iife',
    },
    ...BUNDLING_OPTIONS,
  },
];
