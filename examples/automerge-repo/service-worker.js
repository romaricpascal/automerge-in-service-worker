// Firefox is not heavy on forwarding logs from Service Workers
// to the main window, so instead, we'll use a BroadcastChannel
// to help debugging. A bit blunt, but that'll do
const channel = new BroadcastChannel('service-worker-log');
const originalLog = console.log;
console.log = function (...args) {
  try {
    channel.postMessage(args);
  } catch (error) {
    // There'll be one point where I'll try to log something
    // that's not serialisable for `postMessage` so that'll
    // avoid crashing.
    originalLog(...args);
  }
};

// Import the bundled script for automerge, which will add an `AutomergePromise`
// allowing to wait for the automerge API once all the event listeners
// of the service worker have been set up synchronously
self.importScripts('../../dist/automerge-repo-service-worker.js');
self.importScripts('../../dist/automerge-repo-storage-indexeddb.js');

// Little trickery to have a way to resolve the promise only late
// With that pattern, we get a reference we can pass to `waitUntil`
// or use as a way to delay responses to `isReady`, while delaying
// when the Promise is resolved till the Automerge API has been loaded
// Thankfully the function passed to the Promise constructor
// runs synchronously, allowing to register the listeners OK
const ready = new Promise((markReady) => {
  self.addEventListener('install', (event) => {
    console.log('Installing service worker', new Date().toISOString());
    self.skipWaiting();

    event.waitUntil(ready);
  });

  self.addEventListener('activate', () => {
    console.log('Service worker activated');
  });

  self.addEventListener('message', (event) => {
    console.log('Message from page', event);
    const handler = MESSAGE_HANDLERS[event.data.action];
    if (handler) {
      handler(event);
    }
  });

  const MESSAGE_HANDLERS = {
    async isReady(event) {
      console.log('Checking if server is ready', ready);
      await ready;
      event.source.postMessage({ action: 'ready' });
    },
  };

  // This need to happen only after all event listeners are set up
  // This needs to run in the main script as there's no controlling
  // when the service worker script will be torn down and started again
  // Without this, when we close the browser, `Repo` and `doc`
  // would be undefined, which is less than ideal :(
  (async function () {
    const { Repo } = await AutomergeRepoPromise;
    const { IndexedDBStorageAdapter } =
      await AutomergeRepoStorageIndexedDBPromise;
    const repo = new Repo({
      storage: new IndexedDBStorageAdapter(),
      network: [],
    });

    // To make sure we get the same document for each run of the service worker
    // we need to store its URL somewhere. For that we'll need an IndexedDB database
    // as it's the only other storage than `Cache` available within Service Workers.
    // We'll give it a `settings` table where we can store objects
    const db = await openDb('automerge-service-worker', 1, function (event) {
      event.target.result.createObjectStore('settings', { keyPath: 'key' });
    });
    const rootDocumentUrl = await get(db, 'settings', 'rootDocumentUrl');
    console.log('Root document URL', rootDocumentUrl);
    let handle;
    if (!rootDocumentUrl) {
      handle = repo.create();
      await add(db, 'settings', 'rootDocumentUrl', handle.url);
      await handle.whenReady();
      handle.change((draft) => {
        draft.value = 1;
      });
    } else {
      handle = repo.find(rootDocumentUrl.value);
      await handle.whenReady();
    }

    Object.assign(MESSAGE_HANDLERS, {
      getValue(event) {
        event.source.postMessage({
          action: 'setValue',
          value: handle.docSync().value,
        });
      },
      increment(event) {
        const { value } = event.data;
        handle.change((draft) => {
          draft.value += value;
        });
        console.log('Sending back', handle.docSync().value);
        event.source.postMessage({
          action: 'setValue',
          value: handle.docSync().value,
        });
      },
    });

    console.log('Marking server as ready');
    markReady();
  })();
});

// Very thin wrapper over a few IndexDB APIs to use them as promises
function openDb(name, version, migrate) {
  const request = self.indexedDB.open(name, version);
  request.onupgradeneeded = migrate;
  return promiseForRequest(request);
}

function get(db, storeName, key) {
  const transaction = db.transaction([storeName]);
  const store = transaction.objectStore(storeName);
  return promiseForRequest(store.get(key));
}

function promiseForRequest(request) {
  return new Promise((resolve, reject) => {
    request.onerror = reject;
    request.onsuccess = () => resolve(request.result);
  });
}

function add(db, storeName, key, value) {
  const transaction = db.transaction([storeName], 'readwrite');
  const store = transaction.objectStore(storeName);
  return promiseForRequest(store.add({ key, value }));
}
